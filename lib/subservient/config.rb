module Subservient
  @@namespace = 'subservient'
  def self.namespace
    @@namespace
  end
  def self.namespace= value
    @@namespace = value
  end


  @@pool_urls = ['localhost:11300']
  def self.pool
    @@pool_urls
  end
  def self.pool= value
    @@pool_urls = value
  end
end
