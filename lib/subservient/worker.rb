module Subservient
  class Worker
    attr_reader :tube, :state
    def initialize tube='default'
      @tube    = [Subservient::namespace, tube].join('.')
      @state   = :stopped
      @thread_pool = []
    end

    def go threads: 5
      start threads: threads

      trap('SIGINT') do
        exit
      end
      at_exit do
        Thread.main.wakeup
        stop
      end
      sleep
    end
    def start threads: 5
      raise MissingTaskError if Subservient::tasks.empty?
      raise AlreadyRunningError if @state != :stopped
      @state = :started
      threads.times do
        start_worker_thread
      end
    end
    def stop timeout: 30
      @state = :shutdown        # Tell worker threads not to accept any more jobs
      @thread_pool.each do |thread|
        thread.join(timeout)    # Wait for all workers to exit gracefully...
        thread.kill             # but kill them if they take too long
      end
      @state = :stopped
    end

    private

    def start_worker_thread
      @thread_pool << Thread.new do
        conn = Beaneater::Pool.new(Subservient::pool)
        begin
          work conn
        ensure
          conn.close
        end
      end
    end

    def work conn
      puts "Worker started"
      conn.tubes.watch!(@tube)
      while @state == :started do
        begin
          # Increasing this will decrease load on beanstalkd, but will
          # also increase the time it takes to shut down the worker
          timeout = 2

          bs_job = conn.tubes.reserve(timeout)
          begin
            job = Job.new bs_job
            job.execute
          rescue JobError => e
            puts "#{e} - BURYING JOB #{bs_job.id}"
            bs_job.bury
          end
        rescue Beaneater::TimedOutError
        end
      end
      puts "Worker stopped"
    end

  end
end
