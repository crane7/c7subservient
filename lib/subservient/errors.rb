module Subservient
  class SubservientError    < RuntimeError;     end # Never raised directly

  class WorkerError         < SubservientError; end # Never raised directly
  class AlreadyRunningError < WorkerError;      end # Raised if worker running when Worker#start called

  class JobError            < SubservientError; end # Never raised directly
  class MalformedJobError   < JobError;         end # Raised if job from BS is malformed
  class MissingTaskError    < JobError;         end # Raised if task cannot be found

  class TaskError           < SubservientError; end # Never raised directly
  class RetryJob            < TaskError;            # Raised to retry a job
    attr_accessor :max_retries
    def initialize(max_retries=nil)
      @max_retries = max_retries
    end
  end
  class BuryJob             < TaskError;        end # Raised to bury a job
end
