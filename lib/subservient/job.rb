require 'json'

module Subservient
  class Job
    attr_reader :bs_job, :input, :task, :task_method

    def initialize bs_job
      @bs_job      = bs_job
      @input       = JSON.parse (@bs_job.body)

      task_str     = @input.fetch('task')
      arr          = task_str.rpartition('#').reject { |e| e.empty? or e == '#' }
      @task_method = (arr[1] || :process).to_sym
      task_class   = Object.const_get(arr[0])
      raise MissingTaskError unless task_class.ancestors.include? Subservient::Task
      @task        = task_class.new self
      raise MissingTaskError unless @task.respond_to? @task_method
    rescue JSON::JSONError, KeyError
      raise MalformedJobError
    rescue NameError
      raise MissingTaskError
    end

    def execute
      @task.execute_job
      @bs_job.delete
    rescue RetryJob => e
      if e.max_retries and e.max_retries < @bs_job.stats.retries
        puts "#{e.inspect} - RELEASING JOB #{@job.bs_job.id}"
        puts e.backtrace
        @bs_job.release
      else
        puts "#{e.inspect} - BURYING JOB (EXCEEDED MAX RETRIES) #{@job.bs_job.id}"
        puts e.backtrace
        @bs_job.bury
      end
    rescue BuryJob => e
      puts "#{e.inspect} - BURYING JOB #{@job.bs_job.id}"
      puts e.backtrace
      @bs_job.bury
    end

  end
end
