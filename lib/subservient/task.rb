require 'json'

module Subservient
  class Task
    attr_reader :job
    def initialize job
      @job = job
      @input = @job.input
    end

    def execute_job
      self.send(@job.task_method)
    rescue NoMethodError
      raise MissingTaskError
    rescue *@@retry_on => e
      raise RetryJob.new @@max_retries
    rescue RetryJob, *@@retry_on => e
      e.max_retries ||= @@max_retries
      raise e
    rescue StandardError => e
      raise BuryJob
    end

    private
    @@retry_on = []
    def self.retry_on err
      @@retry_on << err
    end

    @@max_retries = 3
    def self.max_retries n
      @@max_retries = n
    end
  end


  @@tasks = []
  def self.tasks
    if @@tasks.empty?
      ObjectSpace.each_object(Module) do |m|
        @@tasks << m if m.ancestors.include? Subservient::Task
      end
      @@tasks -= [Subservient::Task]
    end
    return @@tasks
  end
end
