# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'subservient/version'

Gem::Specification.new do |spec|
  spec.name          = "subservient"
  spec.version       = Subservient::VERSION
  spec.authors       = ["David Robertson"]
  spec.email         = ["david@davidr.me"]
  spec.summary       = "Job queue processor using Beanstalkd"
  # spec.description   = "TODO: Write a longer description. Optional."
  spec.homepage      = "http://crane7.co.uk"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "beaneater"

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"
end
