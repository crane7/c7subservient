require 'subservient'
require 'json'

module Subservient
  class Worker
    ## Generate some example jobs
    def generate_example_jobs n=20, clear: true
      conn = Beaneater::Pool.new(Subservient::pool)
      tube = conn.tubes[@tube]
      tube.clear if clear

      n.times do |i|
        push_job tube, "Example#puts_input", data: { n: (i+1) }
        push_job tube, "Example#puts_rand"
      end
      push_job tube, "Example#divide_by_zero"

      conn.close
    end

    def push_job tube, task, input={}
      tube.put ({ task: task }.merge(input).to_json)
    end

  end
end

class Example < Subservient::Task
  def puts_input
    puts @job.input.inspect
  end

  def puts_rand
    puts SecureRandom.hex
  end

  def divide_by_zero
    # This will raise ZeroDivisionError
    puts 1/0
  end
end
